<?php
/*
	CW Tech Student Management System
*/

require_once("../models/config.php");

$timeClocked ='';

if (!securePage($_SERVER['PHP_SELF'])){die();}

if (isset($_REQUEST['punchIn'])) {
	$sql = "INSERT INTO punches (user_id, punch_time, type) VALUES ('{$loggedInUser->user_id}', NOW(), 'in')";
	$mysqli->query($sql);
	$sql = "UPDATE {$db_table_prefix}users SET punched_in='in' WHERE id={$loggedInUser->user_id}";
	$mysqli->query($sql);
}

if (isset($_REQUEST['punchOut'])) {
	// Get the latest punch in
	$sql = "SELECT * FROM punches WHERE user_id='{$loggedInUser->user_id}' AND work_type = 'build' ORDER BY punch_time DESC LIMIT 1";
	$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
	$row 	= $result->fetch_array(MYSQLI_ASSOC);
	$date_a = new DateTime($row['punch_time']);
	$date_b = new DateTime('now');
	$interval = date_diff($date_a,$date_b);

	$currentTime = time();
	$timeInPast = strtotime($row['punch_time']);
	$differenceInSeconds = $currentTime - $timeInPast;

	$timeClocked = "<br>Total time clocked: " . $interval->format('%h:%i:%s') . "<br>";
	if ($row['type'] == 'in') {
		$sql = "INSERT INTO punches (user_id, punch_time, type, time_worked, work_type) 
				VALUES ({$loggedInUser->user_id}, NOW(), 'out', $differenceInSeconds, '{$row['work_type']}')";
		$mysqli->query($sql);
		$sql = "UPDATE {$db_table_prefix}users SET punched_in='OUT' WHERE id={$loggedInUser->user_id}";
		$mysqli->query($sql);
	}
	header('Location:account.php?timeclocked='.$timeClocked);
}

include("../models/header.php");

// Check to see if they are logged in
$result = $mysqli->query("SELECT *FROM ".$db_table_prefix."users WHERE id={$loggedInUser->user_id}");
while ($row = $result->fetch_assoc()) {
	print "<div class=\"k-widget widget\">";
	print "<b>Currently:</b> Punched {$row['punched_in']}<br>";
	if ($row['punched_in'] == 'out') {
		print "<form action=account.php>
					<input type='hidden' name='punchIn' value='true'>
					<input type='hidden' name='work_type' value='build'><input type='submit' class='k-button' value='Punch In'>
			   </form>";		
	} else {
		print "<a class='k-button' href='account.php?punchOut=true'>Punch Out</a>";
	}

	if (isset($_REQUEST['timeclocked'])) print $_REQUEST['timeclocked'];

	print "</div>";
}

// Get total events for season
$sql = "SELECT COUNT(*) AS count FROM event_attendance WHERE user_id={$loggedInUser->user_id}";
$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
$row 	= $result->fetch_array(MYSQLI_ASSOC);
$eventCount = $row['count'];
$eventWidth 		= $eventCount * 20;
if ($eventCount > 100) $eventCount = 100;

// Get the total time this season
$sql = "SELECT sum(time_worked) AS punch_time FROM punches WHERE type='out' AND user_id={$loggedInUser->user_id} ORDER BY punch_time DESC";
$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
$row 	= $result->fetch_array(MYSQLI_ASSOC);
if ($row['punch_time'] > 1) {
	$time 	= secondsToTime($row['punch_time']);
	$yourTime =  "<br><center>Total time clocked for you:<br>{$time}</center>";
	$totalHours = round(($row['punch_time']/3600), 2);
} else {
	$yourTime = "<br><center>You have no time clocked.</center>";
}


// Get the total time this season
$sql = "SELECT sum(time_worked) AS punch_time FROM punches WHERE type='out' ORDER BY punch_time DESC";
$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
$row 	= $result->fetch_array(MYSQLI_ASSOC);
$time 	= secondsToTime($row['punch_time']);

$teamTime = "<br><center>Total time clocked for entire team:<br>{$time}</center><br>";

if (isset($totalHours)) {
	$percentage = $totalHours/100;
	$hourWidth 		= round(($totalHours/100),2);
	$hourWidth = $hourWidth * 100;
	if ($hourWidth < 1) $hourWidth = 1;
} else {
	$totalHours = 0;
	$hourWidth = 0;
}
$sponsorWidth	= 0;
$communityWidth = 0;

?>
	<br>
	<div class='k-widget widget'>
		<table class=widget>
			<tr><td width=50% align=right>Participation:</td><td><?= $totalHours ?>/100</td></tr>
			<tr><td colspan=2 class="grey"><div class='green' style='width: <?= $hourWidth ?>%;'></div></td></tr>
			<tr><td align=right>Event:</td><td><?=$eventCount?>/5</td></tr>
			<tr><td colspan=2 class="grey"><div class='blue' style='width: <?= $eventWidth ?>%;'></div></td></tr>
			<tr><td align=right>Sponsorship Level 1:</td><td>/20</td></tr>
			<tr><td colspan=2 class="grey"><div class='red' style='width: <?= $sponsorWidth ?>;'></div></td></tr>
			<tr><td align=right>Sponsorship Level 2:</td><td>xx/50</td></tr>
			<tr><td colspan=2 class="grey"><div class='red' style='width: <?= $sponsorWidth ?>;'></div></td></tr>
			<tr><td align=right>Community Service:</td><td>xx/10</td></tr>
			<tr><td colspan=2 class="grey"><div class='purple' style='width: <?= $communityWidth ?>;'></div></td></tr>
		</table>
		<br>
		<?= $yourTime ?>

		<?= $teamTime ?>
		<br>

		<span class=legend>
			<span class='green'>Sponsorshop Points are 1 point per $10 earned.<br></span>
			<span class='blue'>Participation is 1 point per hour worked.<br></span>
			<span class='red'>Event points are 1 point per event day.<br></span>
			<span class='purple'>Community Service Points are 1 point per hour.</span>
		</span>

		<span class=legend>
			To earn your varsity letter you need: 500 sponsorship points, 100 participation points and 10 community service points.
		</span>
	</div>
<?

include("../models/footer.php");


