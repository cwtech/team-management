<?php
/*
	CW Tech Student Management System
*/

require_once("../models/config.php");

$timeClocked ='';
$userId = $loggedInUser->user_id;

if (!securePage($_SERVER['PHP_SELF'])){die();}

include("../models/header.php");

if (isset($_REQUEST['event_id'])) {
	$event_id = $_REQUEST['event_id'];
	// Unset events for user
	$sql = "DELETE FROM event_attendance WHERE user_id = $userId";
	$mysqli->query($sql);
	foreach($event_id AS $value) {
		$sql = "INSERT INTO event_attendance (user_id, event_id) VALUES ($userId, $value)";
		$mysqli->query($sql);
	}
}

$sql = "SELECT * FROM event_attendance WHERE user_id=$userId";
$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()) {
	$checkbox[$row['event_id']] = ' checked ';
}

print "<center>Check the events attended</center><form>";

// Check to see if they are logged in
$result = $mysqli->query("SELECT *FROM events ORDER BY date");
while ($row = $result->fetch_assoc()) {
	print "<div class=\"k-widget widget\" style='text-align: left;'>";
	print "<table width=100%><tr><td>";
	if (isset($checkbox[$row['id']])) {
		print "<input type='checkbox' name='event_id[]' value='{$row['id']}' checked>";
	} else {
		print "<input type='checkbox' name='event_id[]' value='{$row['id']}'>";
	}
	print "</td><td width=100%>";
	if (isset($row['permissionSlip'])) print "<span style='float: right;'><a href=/permissionSlip/{$row['permissionSlip']} class=k-button>Download Permssion Slip</a></span>";
	print "{$row['name']}<br>{$row['date']}<br>";
	print "<a href='http://{$row['link']}'>Website</a><br>";
	print "</td></tr></table></div><br>";
}
print "<center><input type=submit value='Update Event Attendance' class=k-button></center></form>";
include("../models/footer.php");


