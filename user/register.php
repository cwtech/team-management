<?php


require_once("../models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

// Get the list of schools
$result = $mysqli->query("SELECT * FROM schools ORDER BY school_name");
while ($row = $result->fetch_assoc()) {
	$schools[$row['id']] = $row['school_name'];
}

//Prevent the user visiting the logged in page if he/she is already logged in
if(isUserLoggedIn()) { header("Location: account.php"); die(); }

$userCreated = 0;

//Forms posted
if(!empty($_POST))
{
	$errors = array();
	$email = trim($_POST["email"]);
	$username = trim($_POST["username"]);
	$displayname = trim($_POST["displayname"]);
	$password = trim($_POST["password"]);
	$confirm_pass = trim($_POST["passwordc"]);
	$captcha = md5($_POST["captcha"]);
	$school = trim($_POST['school']);
	
	if ($captcha != $_SESSION['captcha'])
	{
		$errors[] = lang("CAPTCHA_FAIL");
	}
	if(minMaxRange(5,25,$username))
	{
		$errors[] = lang("ACCOUNT_USER_CHAR_LIMIT",array(5,25));
	}
	$aValid = array('-', '_', ' '); 
	if(!ctype_alnum($username)){
		$errors[] = lang("ACCOUNT_USER_INVALID_CHARACTERS");
	}
	if(minMaxRange(3,25,$displayname))
	{
		$errors[] = lang("ACCOUNT_DISPLAY_CHAR_LIMIT",array(5,25));
	}
	if(!ctype_alnum(str_replace($aValid, '', $displayname))){
		$errors[] = lang("ACCOUNT_DISPLAY_INVALID_CHARACTERS");
	}
	if(minMaxRange(8,50,$password) && minMaxRange(8,50,$confirm_pass))
	{
		$errors[] = lang("ACCOUNT_PASS_CHAR_LIMIT",array(8,50));
	}
	else if($password != $confirm_pass)
	{
		$errors[] = lang("ACCOUNT_PASS_MISMATCH");
	}
	if(!isValidEmail($email))
	{
		$errors[] = lang("ACCOUNT_INVALID_EMAIL");
	}
	//End data validation
	if(count($errors) == 0)
	{	
		//Construct a user object
		$user = new User($username,$displayname,$password,$email,$school);
		
		//Checking this flag tells us whether there were any errors such as possible data duplication occured
		if(!$user->status)
		{
			if($user->username_taken) $errors[] = lang("ACCOUNT_USERNAME_IN_USE",array($username));
			if($user->displayname_taken) $errors[] = lang("ACCOUNT_DISPLAYNAME_IN_USE",array($displayname));
			if($user->email_taken) 	  $errors[] = lang("ACCOUNT_EMAIL_IN_USE",array($email));		
		}
		else
		{
			//Attempt to add the user to the database, carry out finishing  tasks like emailing the user (if required)
			if(!$user->userCakeAddUser())
			{
				if($user->mail_failure) $errors[] = lang("MAIL_ERROR");
				if($user->sql_failure)  $errors[] = lang("SQL_ERROR");
			} else {
				$userCreated = 1;
			}
		}
	}
	if(count($errors) == 0) {
		$successes[] = $user->success;
		$userCreated = 1;
	}
}

require_once("../models/header.php");
echo "
<div id='main'>";

echo resultBlock($errors,$successes);

if ($userCreated != 1) {
	echo "
	<div id='regbox' class='k-block' style='width: 500px; margin: 0 auto;'>
	<form name='newUser' action='".$_SERVER['PHP_SELF']."' method='post'>
	<table align=center>
		<tr>
			<td><label>User Name:</label></td>
			<td><input type='text' name='username' class=k-textbox></td>
		</tr>
		<tr>
			<td><label>Display Name:</label></td>
			<td><input type='text' name='displayname' class=k-textbox></td>
		</tr>
		<tr>
			<td><label>Password:</label></td>
			<td><input type='password' name='password' class=k-textbox></td>
		</tr>
		<tr>
			<td><label>Confirm:</label></td>
			<td><input type='password' name='passwordc' class=k-textbox></td>
		</tr>
		<tr>
			<td><label>Email:</label></td>
			<td><input type='text' name='email' class=k-textbox></td>
		</tr>	
		<tr><td><label>School:</label></td>
			<td><select class='dropdown' name='school'>
				<option value='0'>Choose School</option>";

		foreach ($schools AS $key => $value) {
			$selected = '';
			if ($value == $loggedInUser->school) $selected = " selected";

			print "<option value=$key $selected>{$value}</option>";
		}

		print "
				</select></td>
		</tr>
		<tr>
			<td><label>Security Code:</label></td>
			<td><img src='../models/captcha.php'></td>
		</tr>
		<tr>
			<td><label>Enter Security Code:</label></td>
			<td><input name='captcha' type='text' class=k-textbox></td>
		</tr>
		<tr>
			<td colspan=2 align=center><input type='submit' value='Register' class=k-button></td>
		</tr>
	</table>

	</form>
	</div>

	</div>
	<div id='bottom'></div>
	</div>
	</body>
	</html>";
}
?>
