<?php


require_once("../models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

// Get the list of schools
$schools[0] = '';
$result = $mysqli->query("SELECT * FROM schools ORDER BY school_name");
while ($row = $result->fetch_assoc()) {
	$schools[$row['id']] = $row['school_name'];
}

//Prevent the user visiting the logged in page if he is not logged in
if(!isUserLoggedIn()) { header("Location: login.php"); die(); }

if(!empty($_POST))
{
	// Create blank arrays
	$errors = array();
	$successes = array();

	$password = $_POST["password"];
	$password_new = $_POST["passwordc"];
	$password_confirm = $_POST["passwordcheck"];
	
	$errors = array();
	$email = $_POST["email"];
	
	//Perform some validation
	//Feel free to edit / change as required
	
	//Confirm the hashes match before updating a users password
	$entered_pass = generateHash($password,$loggedInUser->hash_pw);
	
	if (trim($password) == ""){
		$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
	}
	else if($entered_pass != $loggedInUser->hash_pw)
	{
		//No match
		$errors[] = lang("ACCOUNT_PASSWORD_INVALID");
	}	
	if($email != $loggedInUser->email)
	{
		if(trim($email) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_EMAIL");
		}
		else if(!isValidEmail($email))
		{
			$errors[] = lang("ACCOUNT_INVALID_EMAIL");
		}
		else if(emailExists($email))
		{
			$errors[] = lang("ACCOUNT_EMAIL_IN_USE", array($email));	
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			$loggedInUser->updateEmail($email);
			$successes[] = lang("ACCOUNT_EMAIL_UPDATED");
		}
	}
	
	if ($password_new != "" OR $password_confirm != "")
	{
		if(trim($password_new) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_NEW_PASSWORD");
		}
		else if(trim($password_confirm) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_CONFIRM_PASSWORD");
		}
		else if(minMaxRange(8,50,$password_new))
		{	
			$errors[] = lang("ACCOUNT_NEW_PASSWORD_LENGTH",array(8,50));
		}
		else if($password_new != $password_confirm)
		{
			$errors[] = lang("ACCOUNT_PASS_MISMATCH");
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			//Also prevent updating if someone attempts to update with the same password
			$entered_pass_new = generateHash($password_new,$loggedInUser->hash_pw);
			
			if($entered_pass_new == $loggedInUser->hash_pw)
			{
				//Don't update, this fool is trying to update with the same password Â¬Â¬
				$errors[] = lang("ACCOUNT_PASSWORD_NOTHING_TO_UPDATE");
			}
			else
			{
				//This function will create the new hash and update the hash_pw property.
				$loggedInUser->updatePassword($password_new);
				$successes[] = lang("ACCOUNT_PASSWORD_UPDATED");
			}
		}
	}

	// Update Custom Fields
	// School
	if (isset($_POST['school']) && count($errors) == 0) {
		$school = $_POST['school'];
		$sql = "UPDATE {$db_table_prefix}users SET school_id='$school' WHERE id='{$loggedInUser->user_id}'";
		$mysqli->query($sql);
		$successes[] = "School Successfully Updated";
		$loggedInUser->school = $schools[$school];
	}

	if(count($errors) == 0 AND count($successes) == 0){
		$errors[] = lang("NOTHING_TO_UPDATE");
	}
}

require_once("../models/header.php");

echo "
<div id='main'>";

echo resultBlock($errors,$successes);

?>

<div id='regbox'>
<form name='updateAccount' action='<?= $_SERVER['PHP_SELF'] ?>' method='post'>
<table align="center" class="k-widget" style="padding: 10px;">
	<tr><td><label>Password:</label></td><td><input type='password' name='password' class="k-textbox"></td></tr>
	<tr><td><label>Email:</label></td><td><input type='text' name='email' value='<?= $loggedInUser->email; ?>' class="k-textbox"></td></tr>
	<tr><td><label>New Pass:</label></td><td><input type='password' name='passwordc' class="k-textbox"></td></tr>
	<tr><td><label>Confirm Pass:</label></td><td><input type='password' name='passwordcheck' class="k-textbox"></td></tr>
	<tr><td><label>School:</label></td>
		<td><select class='dropdown' name='school'>
			<option value='0'>Choose School</option>
<?
	foreach ($schools AS $key => $value) {
		$selected = '';
		if ($value == $loggedInUser->school) $selected = " selected";

		print "<option value=$key $selected>{$value}</option>";
	}
?>
			</select></td>
	</tr>
	<tr><td colspan="2" align="center"><input type='submit' value='Update' class='submit k-button'></td></tr>
</table>
</form>
</div>
</div>
<div id='bottom'></div>
</div>
</body>
</html>
