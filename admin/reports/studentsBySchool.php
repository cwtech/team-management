<?

$output = array();
$maxHoursWorked = 0;

$sql = "SELECT * FROM schools ORDER BY school_name";
$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()) {
    $output[$row['school_name']] = 0;
}

$sql = "SELECT uc_users.school_id, 
            schools.school_name, 
            uc_user_permission_matches.permission_id
        FROM uc_users INNER JOIN schools ON uc_users.school_id = schools.id
             INNER JOIN uc_user_permission_matches ON uc_users.id = uc_user_permission_matches.id";
$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()) {
    if ($row['permission_id'] == 1) {
        $output[$row['school_name']] += 1;
    }
}

print "<br><div style='width: 500px; margin: 0 auto;'><table class='grid' width=500 align=center>";
print "<thead><tr>";
print "<th>School</th>";
print "<th>Number of Students</th>";
print "</tr>";
print "</thead>";

foreach ($output AS $key => $value) {
    print "<tr><td><b>{$key}</b></td><td>{$value}</td></tr>";
}
print "</table></div>";

?>
<script language="javascript">
    $(document).ready(function () {
        setTimeout(function() { $('.grid').kendoGrid(); }, 0);
    });
</script>