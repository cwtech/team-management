<?

$output = '';
$maxHoursWorked = 0;

$sql = "SELECT *, uc_users.user_name, schools.school_name AS school_name FROM punches 
        INNER JOIN uc_users ON punches.user_id = uc_users.id 
        LEFT JOIN schools ON uc_users.school_id = schools.id
        WHERE type='out'";

$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()) {
	if (($row['time_worked'] / 3600) > $maxHoursWorked) $maxHoursWorked = $row['time_worked'] / 3600;
    if (!$row['school_name']) $row['school_name'] = 'No School';
	if (isset($output["{$row['user_name']} ({$row['school_name']})"])) {
		$output["{$row['user_name']} ({$row['school_name']})"] += ($row['time_worked'] / 3600);
	} else {
		$output["{$row['user_name']} ({$row['school_name']})"] = ($row['time_worked'] / 3600);
	}
}

$students = '';
$totalHours = '';
foreach ($output AS $key => $value) {
	if ($students != '') 	$students .= ', ';
	if ($totalHours != '') 	$totalHours .= ', ';
	$students .= "'$key'";
	$totalHours .= "'$value'";
}

?>

	<div id="chart"></div>

    <script>
        function createChart() {
        	console.log('createChart');
            $("#chart").kendoChart({
                title: {
                    text: "Hours by Student"
                },
                legend: {
                    visible: false
                },
                seriesDefaults: {
                    type: "bar"
                },
                series: [{
                    name: "Total Hours",
                    data: [<?= $totalHours ?>]
                }],
                valueAxis: {
                    max: <?= $maxHoursWorked ?>,
                    line: {
                        visible: false
                    },
                    minorGridLines: {
                        visible: true
                    }
                },
                categoryAxis: {
                    categories: [<?= $students ?>],
                    majorGridLines: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    template: "#= series.name #: #= value #"
                }
            });
        }

        $(document).ready(createChart);
        </script>
