<?php

require_once("../models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

include("../models/header.php");

?>

<center>
    <a href="reports.php?report=hoursWorked" class="k-button">Hours Worked</a> 
    <a href="reports.php?report=studentsBySchool" class="k-button">Students by School</a> 
</center>

<?

if (isset($_REQUEST['report'])) {
    require_once("reports/{$_REQUEST['report']}.php");
}

include("../models/footer.php");