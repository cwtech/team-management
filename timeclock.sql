/*
 Navicat Premium Data Transfer

 Source Server         : Local Server
 Source Server Type    : MySQL
 Source Server Version : 50534
 Source Host           : 127.0.0.1
 Source Database       : timeclock

 Target Server Type    : MySQL
 Target Server Version : 50534
 File Encoding         : utf-8

 Date: 10/27/2014 17:53:50 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `event_attendance`
-- ----------------------------
DROP TABLE IF EXISTS `event_attendance`;
CREATE TABLE `event_attendance` (
  `user_id` int(7) DEFAULT NULL,
  `event_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `event_attendance`
-- ----------------------------
BEGIN;
INSERT INTO `event_attendance` VALUES ('3', '1'), ('1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `events`
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` text,
  `permissionSlip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `events`
-- ----------------------------
BEGIN;
INSERT INTO `events` VALUES ('1', 'West Michigan Robotics Invitationsal', '2014-10-25', 'www.wmri.info', '<div><b>Our purpose -- To provide a fun, low-cost event which allows teams to:</b></div>\n<ul>\n<li>enjoy using their robots again.</li>\n<li>give more students an opportunity to drive their robot.</li>\n<li>expose new students (families, sponsors, etc.) to a FIRST competition.</li></ul>\n\n<div><b>Site: Zeeland West High School</b><br>\n3390 100th Ave.<br>\nZeeland, MI 49464 <a href=\"http://www.wmri.info/directions\">(map &amp; directions)</a>\n</div>', 'wmri_permission_2015.docx'), ('2', 'St. Joseph District Day 1', '2015-03-20', 'www.thebluealliance.com/event/2015misjo', null, null), ('3', 'St. Joseph District Day 1', '2015-03-20', 'www.thebluealliance.com/event/2015misjo', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `punches`
-- ----------------------------
DROP TABLE IF EXISTS `punches`;
CREATE TABLE `punches` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(7) DEFAULT NULL,
  `punch_time` datetime DEFAULT NULL,
  `type` enum('in','out') DEFAULT NULL,
  `time_worked` int(11) DEFAULT NULL,
  `work_type` enum('event','build','presentation','sponsor') DEFAULT 'build',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `punches`
-- ----------------------------
BEGIN;
INSERT INTO `punches` VALUES ('9', '1', '2014-10-20 16:01:05', 'in', null, 'build'), ('12', '1', '2014-10-20 16:30:15', 'out', '1750', 'build'), ('13', '1', '2014-10-20 16:37:49', 'in', null, 'build'), ('14', '1', '2014-10-20 16:38:00', 'out', '11', 'build'), ('15', '1', '2014-10-20 16:49:17', 'in', null, 'build'), ('16', '1', '2014-10-20 16:50:10', 'out', '53', 'build'), ('17', '1', '2014-10-20 16:50:24', 'in', null, 'build'), ('18', '1', '2014-10-20 16:50:53', 'out', '29', 'build'), ('19', '1', '2014-10-20 16:51:01', 'in', null, 'build'), ('20', '1', '2014-10-20 16:51:04', 'out', '3', 'build'), ('21', '1', '2014-10-20 16:52:23', 'in', null, 'build'), ('22', '1', '2014-10-20 16:52:26', 'out', '3', 'build'), ('23', '1', '2014-10-20 16:52:42', 'in', null, 'build'), ('24', '1', '2014-10-20 16:52:45', 'out', '3', 'build'), ('25', '1', '2014-10-20 16:53:10', 'in', null, 'build'), ('26', '1', '2014-10-20 16:53:13', 'out', '3', 'build'), ('27', '1', '2014-10-20 16:54:10', 'in', null, 'build'), ('28', '1', '2014-10-20 16:54:13', 'out', '3', 'build'), ('29', '1', '2014-10-20 17:33:11', 'in', null, 'build'), ('30', '1', '2014-10-20 18:54:02', 'in', null, 'build'), ('31', '1', '2014-10-20 18:54:11', 'out', '9', 'build'), ('32', '1', '2014-10-20 20:48:10', 'in', null, 'build'), ('33', '0', '2014-10-20 20:48:46', 'in', null, 'build'), ('34', '0', '2014-10-20 22:48:15', 'in', null, 'build'), ('35', '1', '2014-10-21 06:17:56', 'out', '34186', 'build'), ('36', '1', '2014-10-21 22:23:17', 'in', null, 'build'), ('37', '1', '2014-10-21 22:23:26', 'out', '9', 'build'), ('38', '3', '2014-10-23 11:11:31', 'in', null, 'build'), ('39', '3', '2014-10-23 11:14:13', 'in', null, 'build'), ('40', '3', '2014-10-23 11:14:34', 'in', null, 'build'), ('41', '3', '2014-10-23 11:15:57', 'in', null, 'build'), ('42', '3', '2014-10-23 11:17:07', 'in', null, 'build'), ('43', '3', '2014-10-23 11:17:44', 'out', '37', 'build'), ('44', '3', '2014-10-23 11:22:11', 'in', null, 'build'), ('45', '3', '2014-10-23 11:36:26', 'out', '855', 'build'), ('46', '0', '2014-10-26 01:40:24', 'in', null, 'build'), ('47', '3', '2014-10-27 07:01:41', 'in', null, 'build'), ('48', '3', '2014-10-27 12:58:32', 'out', '21411', 'build'), ('49', '11', '2014-10-27 13:00:26', 'in', null, 'build'), ('50', '11', '2014-10-27 13:00:37', 'out', '11', 'build'), ('51', '11', '2014-10-27 17:20:20', 'in', null, 'build'), ('52', '11', '2014-10-27 17:53:02', 'out', '1962', 'build');
COMMIT;

-- ----------------------------
--  Table structure for `schools`
-- ----------------------------
DROP TABLE IF EXISTS `schools`;
CREATE TABLE `schools` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `schools`
-- ----------------------------
BEGIN;
INSERT INTO `schools` VALUES ('1', 'Coloma High School'), ('2', 'Watervliet High School'), ('3', 'Lake Michigan Catholic High School'), ('4', 'Grace Christian High School'), ('5', 'Michigan Lutheran High School'), ('6', 'Eau Claire High School');
COMMIT;

-- ----------------------------
--  Table structure for `uc_configuration`
-- ----------------------------
DROP TABLE IF EXISTS `uc_configuration`;
CREATE TABLE `uc_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `value` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `uc_configuration`
-- ----------------------------
BEGIN;
INSERT INTO `uc_configuration` VALUES ('1', 'website_name', 'CW Tech Robotarians'), ('2', 'website_url', 'timeclock.localhost/'), ('3', 'email', 'rich@five-star.com'), ('4', 'activation', 'false'), ('5', 'resend_activation_threshold', '0'), ('6', 'language', 'models/languages/en.php'), ('7', 'template', 'models/site-templates/default.css');
COMMIT;

-- ----------------------------
--  Table structure for `uc_pages`
-- ----------------------------
DROP TABLE IF EXISTS `uc_pages`;
CREATE TABLE `uc_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(150) NOT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `uc_pages`
-- ----------------------------
BEGIN;
INSERT INTO `uc_pages` VALUES ('3', 'admin_configuration.php', '1'), ('4', 'admin_page.php', '1'), ('5', 'admin_pages.php', '1'), ('6', 'admin_permission.php', '1'), ('7', 'admin_permissions.php', '1'), ('8', 'admin_user.php', '1'), ('9', 'admin_users.php', '1'), ('10', 'reports.php', '0');
COMMIT;

-- ----------------------------
--  Table structure for `uc_permission_page_matches`
-- ----------------------------
DROP TABLE IF EXISTS `uc_permission_page_matches`;
CREATE TABLE `uc_permission_page_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `uc_permission_page_matches`
-- ----------------------------
BEGIN;
INSERT INTO `uc_permission_page_matches` VALUES ('5', '2', '3'), ('6', '2', '4'), ('7', '2', '5'), ('8', '2', '6'), ('9', '2', '7'), ('10', '2', '8'), ('11', '2', '9');
COMMIT;

-- ----------------------------
--  Table structure for `uc_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `uc_permissions`;
CREATE TABLE `uc_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `uc_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `uc_permissions` VALUES ('1', 'Student'), ('2', 'Administrator');
COMMIT;

-- ----------------------------
--  Table structure for `uc_user_permission_matches`
-- ----------------------------
DROP TABLE IF EXISTS `uc_user_permission_matches`;
CREATE TABLE `uc_user_permission_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `uc_user_permission_matches`
-- ----------------------------
BEGIN;
INSERT INTO `uc_user_permission_matches` VALUES ('1', '1', '2'), ('2', '1', '1'), ('3', '2', '1'), ('4', '3', '1'), ('5', '4', '1'), ('6', '5', '1'), ('7', '6', '1'), ('8', '7', '1'), ('9', '8', '1'), ('10', '9', '1'), ('11', '10', '1'), ('12', '11', '1');
COMMIT;

-- ----------------------------
--  Table structure for `uc_users`
-- ----------------------------
DROP TABLE IF EXISTS `uc_users`;
CREATE TABLE `uc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(150) NOT NULL,
  `activation_token` varchar(225) NOT NULL,
  `last_activation_request` int(11) NOT NULL,
  `lost_password_request` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `title` varchar(150) NOT NULL,
  `sign_up_stamp` int(11) NOT NULL,
  `last_sign_in_stamp` int(11) NOT NULL,
  `punched_in` enum('in','out') NOT NULL DEFAULT 'out',
  `school_id` int(1) DEFAULT NULL,
  `parent_id` int(7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `uc_users`
-- ----------------------------
BEGIN;
INSERT INTO `uc_users` VALUES ('1', 'admin', 'Administrator', 'f3126df9d58c64ba9d3c17b7f2a24f74dda2a83d397a434709f5fe3f2f6ae24e9', 'rich@five-star.com', '4b072c6296c0ba7ceba4a3a5ff4b25f2', '1413829197', '0', '1', 'New Member', '1413829197', '1414446788', 'out', '1', '0'), ('2', 'quattda', 'DaleQ', '9fdc42d6883ed3a01f8c5ac3017a74dbac3639649f5e34fa96b8a2313eba05a28', 'dquattrin@ccs.coloma.org', 'b821e0f1f6a13d50111bc4ea9fd6dea9', '1413996041', '0', '1', 'New Member', '1413996041', '1414080538', 'out', '0', '0'), ('3', 'hardcopi', 'Rich Lester', '01c971a666844977436ec2ba05b15dfd66e0b3d7838e8f7ea69a037d2ae5b7fea', 'rich@scifilog.com', 'b178c72a729473c3111cef762757880b', '1414072747', '0', '1', 'New Member', '1414072747', '1414429110', 'out', '1', '1'), ('11', 'michelle', 'Michelle Lester', '27e89a8d90319f8cced58f11932fae1ad6992857ffc78c6df6a4063501ebd499e', 'aquasplash@five-star.com', '5710cb95201e4ef9f83c98fcd49e3ab2', '1414423231', '0', '1', 'New Member', '1414423231', '1414446780', 'out', '1', null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
