<?php


require_once("models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

include("models/header.php");

// Get the total time this season
$sql = "SELECT sum(time_worked) AS punch_time FROM punches WHERE type='out' ORDER BY punch_time DESC";
$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
$row 	= $result->fetch_array(MYSQLI_ASSOC);
$time 	= secondsToTime($row['punch_time']);
print "<center>Total time clocked for entire team:<br>{$time}<br><br>";

if(!isUserLoggedIn()) {
	print "
	<div id='regbox' class='k-block' style='width: 250px;'>
		<form name='login' action='/user/login.php' method='post'>
			<p>
			<label>Username:</label>
			<input type='text' name='username' class='k-textbox'>
			</p>
			<p>
			<label>Password:</label>
			<input type='password' name='password' class='k-textbox'>
			</p>
			<p>
			<label>&nbsp;</label>
			<input type='submit' value='Login' class='k-button' class='k-textbox'>
			</p>
		</form>
	</div>";
} else {
	print "<a href='user/account.php' class='k-button'>Go to Account Home</a>";
}


include("models/footer.php");