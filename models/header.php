<?php
/*
	CW Tech Student Management System
*/
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title><?= $websiteName ?></title>
	<meta name="viewport" content="width=550" />

    <link href="http://cdn.kendostatic.com/2014.2.1008/styles/kendo.common.min.css" rel="stylesheet">
    <link href="http://cdn.kendostatic.com/2014.2.1008/styles/kendo.default.min.css" rel="stylesheet" />
    <link href="http://cdn.kendostatic.com/2014.2.1008/styles/kendo.dataviz.min.css" rel="stylesheet" />
    <link href="http://cdn.kendostatic.com/2014.2.1008/styles/kendo.dataviz.default.min.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
    <link href="/styles/style.css" rel="stylesheet" />
	
    <script src="http://cdn.kendostatic.com/2014.2.1008/js/jquery.min.js"></script>
    <script src="http://cdn.kendostatic.com/2014.2.1008/js/kendo.all.min.js"></script>
    <script src='../models/funcs.js' type='text/javascript'></script>
</head>
<div class="topLogo">
	<img src="/images/topLogo.png" style="margin: 0 auto;">
</div>
<?

//Links for logged in user
if(isUserLoggedIn()) {
	echo "<ul class='menu'>
		<li><a href='/user/account.php'>Account Home</a></li>
		<li><a href='/user/events.php'>Events</a></li>
		<li><a href='/user/user_settings.php'>User Settings</a></li>
		<li><a href='/user/logout.php'>Logout</a></li>
		</ul>
	";
	
	if ($loggedInUser->checkPermission(array(2))){
	echo "<ul class='menu'>
				<li><a href='/admin/admin_configuration.php'>Configuration</a></li>
				<li><a href='/admin/admin_users.php'>Users</a></li>
				<li><a href='/admin/admin_permissions.php'>Permissions</a></li>
				<li><a href='/admin/admin_pages.php'>Pages</a></li>
				<li><a href='/admin/reports.php'>Reports</a></li>
			</ul>";
	}
} 
//Links for users not logged in
else {
	echo "<ul class='menu'>
	<li><a href='/index.php'>Home</a></li>
	<li><a href='/user/register.php'>Register</a></li>
	<li><a href='/user/forgot-password.php'>Forgot Password</a></li>
	</ul>";
}
print "</ul><br>";
