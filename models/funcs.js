/*
	CW Tech Student Management System
*/

$(document).ready(function() {
	$('.menu').kendoMenu();
	$('.admin').kendoGrid();
	$('.dropdown').kendoDropDownList();
});

function showHide(div){
	if(document.getElementById(div).style.display = 'block'){
		document.getElementById(div).style.display = 'none';
	}else{
		document.getElementById(div).style.display = 'block'; 
	}
}